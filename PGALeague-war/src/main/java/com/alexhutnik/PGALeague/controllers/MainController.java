package com.alexhutnik.PGALeague.controllers;

import com.alexhutnik.PGALeague.objects.Team;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by alex on 6/22/14.
 */

@Controller
public class MainController {

    private static Logger sLogger = Logger.getLogger("MainController");

    private static String sWeek = "32";
    private static String sPGATOUR_EVENT_ID = "033";
    private static final boolean IS_MAJOR = true;

    @RequestMapping("/")
    public String viewResults(@RequestParam(value = "sortKey", required = false, defaultValue = "WEEK") String theSortKey, ModelMap theModelMap) throws IOException, ParseException {
        final SortKey aSortKey = SortKey.valueOf(theSortKey);

        Map<String, Integer> fGolferPayoffMap = new HashMap<String, Integer>();
        Map<String, Team> fTeamStandings = new HashMap<String, Team>();

        //Call PGATour.com for current leaderboard

        ObjectMapper aMapper = new ObjectMapper();
        JsonNode rootNode = aMapper.readValue(new URL("http://www.pgatour.com/data/r/" + sPGATOUR_EVENT_ID + "/leaderboard-v2.json?ts=" + System.currentTimeMillis()), JsonNode.class);

        for (JsonNode aJsonNode : rootNode.path("leaderboard").path("players")) {
            String aGolfer = aJsonNode.get("player_bio").get("first_name").asText() + " " + aJsonNode.get("player_bio").get("last_name").asText();
            Integer aPayoff = aJsonNode.get("rankings").get("projected_money_event").asInt();
            fGolferPayoffMap.put(aGolfer, aPayoff);
        }

        String aTournamentName = rootNode.path("leaderboard").get("tournament_name").asText();
        theModelMap.addAttribute("tournamentName", aTournamentName);

        String lastUpdated = rootNode.get("last_updated").asText().replace("T", " ");
        theModelMap.addAttribute("lastUpdated", lastUpdated);

        //Call League site for standings

        Document doc = Jsoup.connect("http://71.18.45.74/pga/index.php3?p_pg=standings").timeout(5*1000).get();
        Elements teamRows = doc.select("div#weekdetail table tr.even, div#weekdetail table tr.odd");
        for (Element aTeamRow : teamRows) {
            Team aTeam = new Team();
            aTeam.setTeamName(aTeamRow.select("td").get(2).text());
            aTeam.setTotal(NumberFormat.getNumberInstance(Locale.US).parse(aTeamRow.select("td").get(4).text()).intValue());
            fTeamStandings.put(aTeam.getTeamName(), aTeam);
        }

        //Call League site for picks

        doc = Jsoup.connect("http://71.18.45.74/pga/index.php3?p_pg=week&h_page=results&h_week=" + sWeek).timeout(5*1000).get();
        Elements aTeamElementsList = doc.select("div#weekdetail table");
        for (Element aTeam : aTeamElementsList) {
            String aTeamName = aTeam.select("tr th.team").text();
            Integer winnings = 0;
            for (Element aTeamPick : aTeam.select("tr.odd, tr.even")) {
                String theGolfer = aTeamPick.select("td.none").text();
                if (fGolferPayoffMap.get(theGolfer) == null) {
                    sLogger.log(Level.WARNING, "Golfer not found: " + theGolfer);
                } else {
                    winnings += fGolferPayoffMap.get(theGolfer);
                }
            }

            if(IS_MAJOR){
                winnings = Integer.valueOf((int)Math.round(winnings * 1.5));
            }

            fTeamStandings.get(aTeamName).setThisWeek(winnings);
            fTeamStandings.get(aTeamName).addToTotal(winnings);
        }

        List<Team> aTeamList = new ArrayList<Team>(fTeamStandings.values());
        Collections.sort(aTeamList, new Comparator<Team>() {
            @Override
            public int compare(Team o1, Team o2) {
                switch (aSortKey) {
                    case WEEK:
                        if (o1.getThisWeek().equals(o2.getThisWeek())) {
                            return -1 * o1.getTotal().compareTo(o2.getTotal());
                        }
                        return -1 * o1.getThisWeek().compareTo(o2.getThisWeek());
                    case TOTAL:
                        if (o1.getTotal().equals(o2.getTotal())) {
                            return -1 * o1.getThisWeek().compareTo(o2.getThisWeek());
                        }
                        return -1 * o1.getTotal().compareTo(o2.getTotal());
                    default:
                        return 0;
                }
            }
        });

        //Find number of first place winners
        addBonusToFirstPlaceTeams(aTeamList);
        calculateRanking(aTeamList, aSortKey);

        theModelMap.addAttribute("teamList", aTeamList);

        return "results";
    }

    /**
     * Adds $200k bonus to first place team.
     * @param theTeamList - a Team list
     */
    protected static void addBonusToFirstPlaceTeams(List<Team> theTeamList) {

        int highestAmount = 0;
        List<Integer> firstPlaceTeams = new ArrayList<Integer>();

        //Loop through all the teams looking for the highest amount and keep track of the first place finishers
        for (Team aTeam : theTeamList) {
            if (aTeam.getThisWeek() == highestAmount) {
                firstPlaceTeams.add(theTeamList.indexOf(aTeam));
            } else if(aTeam.getThisWeek() > highestAmount){
                firstPlaceTeams.clear();
                firstPlaceTeams.add(theTeamList.indexOf(aTeam));
                highestAmount = aTeam.getThisWeek();
            }
        }

        //Calculate bonus amount
        int firstPlaceBonus = 200000 / firstPlaceTeams.size();

        //Add bonus to teams
        for (Integer aFirstPlaceTeam : firstPlaceTeams) {
            theTeamList.get(aFirstPlaceTeam).addToThisWeek(firstPlaceBonus);
        }
    }

    protected static void calculateRanking(List<Team> theTeamList, SortKey theSortKey) {

        Integer theCurrentHighestAmount = 0;
        Integer theCurrentHighestRanking = 0;

        for (Team aTeam : theTeamList) {

            Boolean rowEqualityTest = null;

            switch (theSortKey) {
                case WEEK:
                    rowEqualityTest = aTeam.getThisWeek().equals(theCurrentHighestAmount);
                    theCurrentHighestAmount = aTeam.getThisWeek();
                    break;
                case TOTAL:
                    rowEqualityTest = aTeam.getTotal().equals(theCurrentHighestAmount);
                    theCurrentHighestAmount = aTeam.getTotal();
                    break;
            }

            if (rowEqualityTest) {
                aTeam.setRank(theCurrentHighestRanking);
            } else {
                aTeam.setRank(++theCurrentHighestRanking);
            }
        }
    }

    // Total control - setup a model and return the view name yourself. Or consider
    // subclassing ExceptionHandlerExceptionResolver (see below).
    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {
        sLogger.log(Level.SEVERE, "Request: " + req.getRequestURL() + " raised " + exception);

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("url", req.getRequestURL() + (req.getQueryString() == null ? "" : "?" + req.getQueryString()));
        mav.setViewName("error");
        return mav;
    }

    enum SortKey {
        WEEK,
        TOTAL
    }
}
