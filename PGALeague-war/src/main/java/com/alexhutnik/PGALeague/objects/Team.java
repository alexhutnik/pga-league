package com.alexhutnik.PGALeague.objects;

/**
* Created by alex on 6/22/14.
*/
public class Team {
    private String fTeamName;
    private Integer fThisWeek = 0;
    private Integer fTotal = 0;
    private Integer fRank = 0;

    public Team() {
        //empty constructor
    }

    public Team(String theTeamName, Integer theThisWeek, Integer theTotal) {
        fTeamName = theTeamName;
        fThisWeek = theThisWeek;
        fTotal = theTotal;
    }

    public String getTeamName() {
        return fTeamName;
    }

    public void setTeamName(String theTeamName) {
        fTeamName = theTeamName;
    }

    public Integer getThisWeek() {
        return fThisWeek;
    }

    public void setThisWeek(Integer theThisWeek) {
        fThisWeek = theThisWeek;
    }

    public Integer getTotal() {
        return fTotal;
    }

    public void setTotal(Integer theTotal) {
        fTotal = theTotal;
    }

    public Integer getRank() {
        return fRank;
    }

    public void setRank(Integer theRank) {
        fRank = theRank;
    }

    public void addToTotal(Integer theAmount){
        fTotal += theAmount;
    }

    public void addToThisWeek(Integer theAmount){
        fThisWeek += theAmount;
    }
}
