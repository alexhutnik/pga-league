<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 6/22/14
  Time: 3:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>PGA League Projected Results</title>
    <link href="/images/favicon.ico" rel="icon" type="image/x-icon" />
</head>
<style>
    thead {font-weight: bold}
    thead a {color: blue; text-decoration: none}
    td {
        padding-left: 10px;
        padding-right: 10px;
    }

    tr.odd{background-color: #D9FBFF}
    tr.even{background-color: #FFDDD9}
</style>
<body>
<h2>Real-Time Winnings - ${tournamentName}</h2>
<p><em>Does not yet include ringer/tank pick bonuses or BCL penalties.</em></p>
<p><em>Last updated: ${lastUpdated}</em></p>
<table>
    <thead>
    <td>Rank</td>
    <td>Team</td>
    <td><a href="?sortKey=WEEK">Projected Winnings</a></td>
    <td><a href="?sortKey=TOTAL">Projected Total Winnings</a></td>
    </thead>
    <c:forEach items="${teamList}" var="team" varStatus="loopStatus">
        <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}"><td>${team.rank}</td><td>${team.teamName}</td><td><fmt:formatNumber type="currency" value="${team.thisWeek}"/> </td><td><fmt:formatNumber type="currency" value="${team.total}"/></td></tr>
    </c:forEach>
</table>

<p><a href="https://bitbucket.org/alexhutnik/pga-league/issues?status=new&status=open">Bugs</a></p>

</body>
</html>
