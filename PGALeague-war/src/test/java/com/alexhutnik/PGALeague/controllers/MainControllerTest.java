package com.alexhutnik.PGALeague.controllers;

import com.alexhutnik.PGALeague.objects.Team;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by alex on 6/24/14.
 */
public class MainControllerTest {

    @Test
    public void testAddBonus(){
        Team aTeam1 = new Team("a", 10, 25);
        Team aTeam2 = new Team("b", 10, 20);
        Team aTeam3 = new Team("c", 5, 20);

        List<Team> aTeamList = Arrays.asList(new Team[]{aTeam1, aTeam2, aTeam3});

        MainController.addBonusToFirstPlaceTeams(aTeamList);
        Assert.assertEquals((long)100010, (long)aTeamList.get(0).getThisWeek());
        Assert.assertEquals((long)100010, (long)aTeamList.get(1).getThisWeek());
    }

}
